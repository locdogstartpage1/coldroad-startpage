# ColdRoad startpage

a fork of [this](https://github.com/isvaan/isvaan.github.io) well thought, simple startpage. The artwork features reddit user [u/Zzerow](https://www.reddit.com/user/Zzerow/)

<img src="https://gitlab.com/locdog/coldroad-startpage/-/raw/main/ColdRoad/ColdRoadScreenshot.png">

